===============================
PyFortune
===============================


.. image:: https://img.shields.io/pypi/v/pyfortune.svg
        :target: https://pypi.python.org/pypi/pyfortune

.. image:: https://img.shields.io/travis/carmenbbakker/pyfortune.svg
        :target: https://travis-ci.org/carmenbbakker/pyfortune

.. image:: https://readthedocs.org/projects/pyfortune/badge/?version=latest
        :target: https://pyfortune.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/carmenbbakker/pyfortune/shield.svg
     :target: https://pyup.io/repos/github/carmenbbakker/pyfortune/
     :alt: Updates


PyFortune is a program that shows fortune cookies


* Free software: GNU General Public License v3
* Documentation: https://pyfortune.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

