#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from unittest import mock
from unittest.mock import patch


from pyfortune import core

@patch('pyfortune.core.subprocess')
def test_get_fortune(mock_subprocess):
	fortune = b'Nobody expects the Spanish Inquisition\n'
	formatted_fortune = 'Nobody expects the Spanish Inquisition'

	mock_subprocess.check_output.return_value = fortune

	result = core.get_fortune()

	mock_subprocess.check_output.assert_called_with(['fortune'])
	assert result == formatted_fortune
